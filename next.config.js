const path = require('path');
const sitemap = require('nextjs-sitemap-generator');
const withSass = require('@zeit/next-sass');

sitemap({
  baseUrl: 'https://ohmyblogggers.ru',
  pagesDirectory: __dirname + "/pages",
  targetDirectory : './'
});

module.exports = withSass({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  },
  webpack: config => {
    config.resolve.alias['components'] = path.resolve(__dirname, './components');
    config.resolve.alias['data'] = path.resolve(__dirname, './data');
    config.resolve.alias['helpers'] = path.resolve(__dirname, './helpers');
    config.resolve.alias['constants'] = path.resolve(__dirname, './constants');
    return config;
  },
  exportPathMap: function () {
    return {
      '/': { page: '/' },
    }
  }
});
