import React from 'react';

// Components
import { BaseContainer, Heading, Block } from 'components';

// Data
import common from 'data/common';

// styles
import base from '../styles/base.sass';

const Index = ({ common }) => {
  const onScrollToByClick = (alias) => {
    const anchor = document.querySelector(`#${alias}`);
    const y = anchor && anchor.getBoundingClientRect().top + window.scrollY;
    window.scrollTo({
      top: y,
      behavior: 'smooth',
    })
  };

  return (
    <React.Fragment>
      <Heading />

      <div>
        <BaseContainer
          data={common}
          onClick={onScrollToByClick}
        />
        {common && common.blocks && common.blocks.map((item, key) => (
          <Block
            key={key}
            alias={item.alias}
            title={item.title}
            descriptionList={item.list}
          />
        ))}
      </div>
    </React.Fragment>
  )
};

Index.getInitialProps = async () => ({
  common,
});

export default Index;


