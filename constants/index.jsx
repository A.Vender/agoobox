export const CONSTANTS = {
  logo_url: '/static/images/agoobox-logo.svg',
  colors: {
    medicine: 'blue',
    care: 'pink',
    accessories: 'mint',
    toys: 'purple'
  }
};


