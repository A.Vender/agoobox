import React from 'react';
import PropTypes from 'prop-types';

// Components
import { Icon } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.sass';
const cx = classNames.bind(styles);


const Round = ({ color, onClick, iconName, buttonText }) => (
  <button
    className={cx('button')}
    onClick={onClick}
  >
    {iconName && (
      <span className={cx('round_button', color)}>
        <span className={cx('icon')}>
          <Icon name={iconName} />
        </span>
      </span>
    )}

    {buttonText && (
      <span className={cx('buttonText')}>{buttonText}</span>
    )}
  </button>
);

Round.propTypes = {
  buttonText: PropTypes.string,
  iconName: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.func,
};

export default {
  Round,
};
