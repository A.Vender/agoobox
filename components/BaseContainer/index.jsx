import React from 'react';
import PropTypes from 'prop-types';

// Components
import { NativeLink, Menu } from 'components';

// Constants
import { CONSTANTS } from '../../constants';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.sass';
const cx = classNames.bind(styles);

const BaseContainer = ({ onClick, data: {
  title,
  contacts,
  menu,
  about,
} }) => (
  <div className={cx('base')}>
    {menu && (
      <Menu.Mobile menu={menu} />
    )}
    <ul className={cx('contacts')}>
      {contacts.map((item, key) => (
        <li key={key}>
          <NativeLink
            href={item.href}
            name={item.name}
            className={cx('link')}
          />
        </li>
      ))}
    </ul>

    <div className={cx('container')}>
      <div className={cx('title')}>
        <img
          className={cx('logo')}
          src={CONSTANTS.logo_url}
          alt="agoo_box_logo"
        />
        <h1 dangerouslySetInnerHTML={{ __html: title }} />
      </div>

      {menu && (
        <div className={cx('menu')}>
          <Menu.Desktop
            menu={menu}
            onClick={onClick}
          />
        </div>
      )}

      <div
        className={cx('about_box')}
        dangerouslySetInnerHTML={{ __html: about }}
      />
    </div>
  </div>
);

BaseContainer.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    contacts: PropTypes.array,
    menu: PropTypes.array,
  })
};

export default BaseContainer;
