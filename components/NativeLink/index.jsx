import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

const NativeLink = ({ href, className, name }) => (
  <Link href={href}>
    <a className={className}>
      {name}
    </a>
  </Link>
);

NativeLink.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
};

export default NativeLink;
