import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.sass';
const cx = classNames.bind(styles);

const Icon = ({ name, className }) => (
  <div className={cx('icon', name, className)} />
);

Icon.propTypes = {
  name: PropTypes.any,
  className: PropTypes.any,
};

export default Icon;
