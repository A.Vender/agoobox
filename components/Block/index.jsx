import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { CONSTANTS } from '../../constants';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.sass';
const cx = classNames.bind(styles);

const Block = ({ alias, title, descriptionList }) => (
  <div
    id={alias}
    className={cx('block_container', CONSTANTS.colors[alias])}
  >
    <div className={cx('wrapper')}>
      <h2 className={cx('title')}>{title}</h2>
      {descriptionList.map((item, key) => (
        <div key={key} className={cx('box')}>
          <div
            className={cx('subTitle')}
            dangerouslySetInnerHTML={{ __html: item.title }}
          />
          <div
            className={cx('description')}
            dangerouslySetInnerHTML={{ __html: item.description }}
          />
        </div>
      ))}
    </div>
  </div>
);

Block.propTypes = {
  alias: PropTypes.string,
  title: PropTypes.string,
  descriptionList: PropTypes.array
};

export default Block;

