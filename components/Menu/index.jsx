import React from 'react';
import PropTypes from 'prop-types';

// Components
import { Button, NativeLink } from 'components';

// Constants
import { CONSTANTS } from '../../constants';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.sass';
const cx = classNames.bind(styles);

const Desktop = ({ menu, onClick }) => (
  <ul className={cx('menu_desktop')}>
    {menu.map((item, key) => (
      <li key={key} className={cx('menu_button')}>
        <Button.Round
          onClick={() => onClick(item.alias)}
          buttonText={item.name}
          color={CONSTANTS.colors[item.alias]}
          iconName={item.alias}
        />
      </li>
    ))}
  </ul>
);

Desktop.propTypes = {
  menu: PropTypes.array,
  onClick: PropTypes.func
};

const Mobile = ({ menu }) => (
  <div className={cx('menu_mobile')}>
    <ul className={cx('drum')}>
      {menu.map((item, key) => (
        <li key={key}>
          <NativeLink
            className={cx('link')}
            href={`#${item.alias}`}
            name={item.name}
          />
        </li>
      ))}
    </ul>
  </div>
);

Mobile.propTypes = {
  menu: PropTypes.array,
};

export default {
  Desktop,
  Mobile
};
